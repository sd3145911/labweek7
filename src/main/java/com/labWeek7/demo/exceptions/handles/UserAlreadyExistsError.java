package com.labWeek7.demo.exceptions.handles;

public class UserAlreadyExistsError extends RuntimeException{
    public UserAlreadyExistsError(){
        super("User already exists!");
    }
}
