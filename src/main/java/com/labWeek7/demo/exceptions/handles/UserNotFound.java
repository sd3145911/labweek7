package com.labWeek7.demo.exceptions.handles;

public class UserNotFound extends RuntimeException{
    public UserNotFound(){
        super("User not found");
    }
}
