package com.labWeek7.demo.exceptions.exceptionManager;

import com.labWeek7.demo.exceptions.handles.UserAlreadyExistsError;
import com.labWeek7.demo.exceptions.handles.UserNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class Manager {

    @ExceptionHandler(UserAlreadyExistsError.class)
    private ResponseEntity<String> userAlreadyExists(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new UserAlreadyExistsError().getMessage());
    }

    @ExceptionHandler(UserNotFound.class)
    private ResponseEntity<String> userNotFound(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new UserNotFound().getMessage());
    }
}
