package com.labWeek7.demo.repository;

import com.labWeek7.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserRepository extends JpaRepository<User, String> {
    UserDetails findUserByEmail(String email);
}
