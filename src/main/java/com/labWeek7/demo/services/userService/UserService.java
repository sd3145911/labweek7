package com.labWeek7.demo.services.userService;

import com.labWeek7.demo.dtos.AuthenticationDTO;
import com.labWeek7.demo.dtos.LoginResponseDTO;
import com.labWeek7.demo.dtos.register.UserRequestRegisterDTO;
import com.labWeek7.demo.dtos.register.UserResponseRegisterDTO;
import com.labWeek7.demo.entity.User;
import com.labWeek7.demo.exceptions.handles.UserAlreadyExistsError;
import com.labWeek7.demo.exceptions.handles.UserNotFound;
import com.labWeek7.demo.factory.UserFactory;
import com.labWeek7.demo.repository.UserRepository;
import com.labWeek7.demo.services.TokenService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final TokenService tokenService;


    public UserService(UserRepository userRepository, AuthenticationManager authenticationManager, TokenService tokenService) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.tokenService = tokenService;
    }

    public UserResponseRegisterDTO registerUser(UserRequestRegisterDTO requestDTO){
        if(userRepository.findUserByEmail(requestDTO.email()) != null) throw new UserAlreadyExistsError();

        var user = userRepository.save(UserFactory.createNewUser(requestDTO));
        return new UserResponseRegisterDTO(user.getId(), user.getEmail(), user.getRole());
    }

    public LoginResponseDTO login(AuthenticationDTO authenticationDTO){
        var usernamePassword = new UsernamePasswordAuthenticationToken(authenticationDTO.email(), authenticationDTO.password());
        var auth = this.authenticationManager.authenticate(usernamePassword);

        return new LoginResponseDTO(tokenService.generateToken((User) auth.getPrincipal()));
    }

    public void deleteUser(String id){
        if(userRepository.findById(id).isEmpty()) throw new UserNotFound();
        userRepository.deleteById(id);
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }
}
