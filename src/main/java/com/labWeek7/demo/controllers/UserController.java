package com.labWeek7.demo.controllers;

import com.labWeek7.demo.dtos.AuthenticationDTO;
import com.labWeek7.demo.dtos.LoginResponseDTO;
import com.labWeek7.demo.dtos.register.UserRequestRegisterDTO;
import com.labWeek7.demo.dtos.register.UserResponseRegisterDTO;
import com.labWeek7.demo.entity.User;
import com.labWeek7.demo.services.userService.UserService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/auth")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponseRegisterDTO> registerUser(@Valid @RequestBody UserRequestRegisterDTO userRequestRegister){
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.registerUser(userRequestRegister));
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDTO> registerUser(@Valid @RequestBody AuthenticationDTO authenticationDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.login(authenticationDTO));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable String id){
        userService.deleteUser(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/")
    public ResponseEntity<List<User>> deleteUser(){
        return ResponseEntity.status(HttpStatus.OK).body(userService.getAllUsers());
    }
}
