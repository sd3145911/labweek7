package com.labWeek7.demo.factory;

import com.labWeek7.demo.dtos.register.UserRequestRegisterDTO;
import com.labWeek7.demo.entity.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserFactory {

    public static User createNewUser(UserRequestRegisterDTO requestDTO){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = new User();

        user.setEmail(requestDTO.email());
        user.setPassword(encoder.encode(requestDTO.password()));
        user.setRole(requestDTO.role());
        return user;
    }
}
