package com.labWeek7.demo.dtos;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public record AuthenticationDTO(
        @Email @Min(8)
        String email,
        @NotNull @Min(6)
        String password) {
}
