package com.labWeek7.demo.dtos;

public record LoginResponseDTO(String token) {
}
