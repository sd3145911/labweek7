package com.labWeek7.demo.dtos.register;

import com.labWeek7.demo.role.UserRole;

public record UserResponseRegisterDTO(String id, String email, UserRole role) {

}
