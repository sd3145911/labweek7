package com.labWeek7.demo.dtos.register;

import com.labWeek7.demo.role.UserRole;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public record UserRequestRegisterDTO(
        @Email @Min(8) @NotNull
        String email,
        @NotNull @Min(6)
        String password,
        @NotNull
        UserRole role) {

}
